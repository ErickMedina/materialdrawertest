# README #

Este proyecto usa la librería de https://github.com/mikepenz/MaterialDrawer que nos permite usar activities en vez de fragments y que podamos tener el menú lateral en las activities que queramos. 

* Pros

Permite crear menús laterales muy potentes y relativamente de manera fácil.

* Contras

La pega es que hay que crear el menú lateral en cada activity en la que queramos que aparezca, es decir, que hay que crear el mismo menú lateral varias veces.


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact